# Seminar Uebersetzerbau

A Git repo for the seminar paper in "Seminar in Übersetzerbau"

# Setup

1. Latex installieren: `pamac install latex-most latex-bin`
2. Für VSCode LatexWorkshop installieren: https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop
3. Eine `.tex` Datei öffnen
4. Den Command "Latex Build Project" suchen und ausführen

Es sollte jetzt ein PDF gebaut worden sein :)

